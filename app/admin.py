from django.contrib import admin
from app.models import GroupClient, Client, TypeService, LocalisationCountry, LocalisationZone, LocalisationState, Area, ProfilCenter, Service, ClientService


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = [
        'name', 'group_client', 'business_name', 'address_1',
        'identification_number', 'create_at', 'modified_at', 'identification_type',
        'create_at', 'modified_at', 'localisation_country', 'localisation_state', 'localisation_zone',
        'tecnical_contact', 'comercial_contact', 'finance_contact']
    list_filter = ('group_client', )
    search_fields = ('name', )


@admin.register(GroupClient)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['name', 'create_at', 'modified_at']
    pass


@admin.register(TypeService)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['name', 'create_at', 'modified_at']
    pass


@admin.register(LocalisationCountry)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['name', 'iso_code_2', 'iso_code_3', 'status']
    pass


@admin.register(LocalisationZone)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['name', 'code', 'sort_order',
                    'status', 'localisation_country']
    pass


@admin.register(LocalisationState)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['name', 'sort_order', 'status', 'localisation_zone']
    pass


@admin.register(Area)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['name', 'create_at', 'modified_at']
    pass


@admin.register(ProfilCenter)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['name', 'create_at', 'modified_at']
    pass


@admin.register(Service)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['name', 'comment', 'create_at',
                    'modified_at', 'area', 'profil_Center', 'type_service']
    list_filter = ('area', 'type_service')
    search_fields = ('name', )
    pass


@admin.register(ClientService)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['service', 'amount', 'pay_day', 'client', 'profil_Center', 'type_sla', 'type_contract',
                    'start_date', 'end_date']
    list_filter = ('type_sla', 'type_contract', 'client', 'service')
    pass
