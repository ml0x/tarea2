from django.db import models


class GroupClient (models.Model):
    name = models.CharField(max_length=144)
    create_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class TypeService(models.Model):
    name = models.CharField(max_length=144)
    create_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class LocalisationCountry(models.Model):
    name = models.CharField(max_length=144)
    iso_code_2 = models.CharField(max_length=2)
    iso_code_3 = models.CharField(max_length=3)
    sort_order = models.IntegerField()
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.name

class LocalisationZone(models.Model):
    name = models.CharField(max_length=144)
    code = models.CharField(max_length=32)
    sort_order = models.IntegerField()
    status = models.BooleanField(default=True)
    localisation_country = models.ForeignKey(LocalisationCountry, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class LocalisationState(models.Model):
    name = models.CharField(max_length=144)
    sort_order = models.IntegerField()
    status = models.BooleanField(default=True)
    localisation_zone = models.ForeignKey(LocalisationZone, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Area(models.Model):
    name = models.CharField(max_length=144)
    create_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class ProfilCenter(models.Model):
    name = models.CharField(max_length=144)
    create_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class Client(models.Model):
    name = models.CharField(max_length=144)
    business_name = models.CharField(max_length=144)
    address_1 = models.CharField(max_length=144)
    identification_type_choices=(('C', 'RUT'), ('P', 'PASAPORTE'))
    identification_type = models.CharField(max_length=1, choices=identification_type_choices)
    identification_number = models.CharField(max_length=50)
    create_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now_add=True)
    group_client = models.ForeignKey(GroupClient, on_delete=models.CASCADE)
    localisation_country = models.ForeignKey(LocalisationCountry, on_delete=models.CASCADE)
    localisation_state = models.ForeignKey(LocalisationState, on_delete=models.CASCADE)
    localisation_zone = models.ForeignKey(LocalisationZone, on_delete=models.CASCADE)
    tecnical_contact = models.EmailField(null=True, blank=True)
    comercial_contact = models.EmailField(null=True, blank=True)
    finance_contact = models.EmailField(null=True, blank=True)

    def __str__(self):
        return self.name


class Service(models.Model):
    name = models.CharField(max_length=144)
    comment = models.CharField(max_length=1000)
    create_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now_add=True)
    area = models.ForeignKey(Area, on_delete=models.CASCADE)
    profil_Center = models.ForeignKey(ProfilCenter, on_delete=models.CASCADE)
    type_service = models.ForeignKey(TypeService, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class ClientService(models.Model):
    amount = models.IntegerField()
    pay_day = models.IntegerField()
    service = models.ForeignKey(Service, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    profil_Center = models.ForeignKey(ProfilCenter, on_delete=models.CASCADE)
    type_sla_choices = (('T1', '8x5'), ('T2', '24x7'))
    type_sla = models.CharField(max_length=2, choices=type_sla_choices)
    type_contract_choices = (('T1', 'Definido'), ('T2', 'indefinido'))
    type_contract = models.CharField(max_length=2, choices=type_contract_choices)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()



